package com.jsonsurvey;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;

import com.facebook.react.ReactActivity;

public class MainActivity extends AppCompatActivity {

  private Button start;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    start = findViewById(R.id.start);

    start.setOnClickListener(view -> {
      Intent intent = new Intent(MainActivity.this, ReactNativeActivity.class);
      startActivity(intent);
      finish();
    });


  }
}

